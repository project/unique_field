# Unique Field

The Unique Field module provides a way to require that specified fields or
characteristics of a node/taxonomy term/user are unique. This includes the
title, language, taxonomy terms, and other fields.

Without this module, Drupal and CCK do not prevent multiple nodes from
having the same title or the same value in a given field.

For example, if you have a content type with a Title field and there
should only be one node per Title, you could use this module to prevent a
node from being saved with a Title already used in another node. As an
improvement we have implemented unique field for user and taxonomy fields.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/unique_field).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/unique_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Content type 

This module adds additional options to the administration page for each
content type (i.e. admin/structure/types/manage/<content type>) for
specifying which fields must be unique. The administrator may specify
whether each field must be unique or whether the fields in combination
must be unique.
Also, the administrator can choose whether the fields must be unique among all
other nodes or only among nodes from the given node's content type.

Alternatively, you can select the 'single node' scope, which allows you
to require that the specified fields are each unique on that node. For
example, if a node has multiple, separate user reference fields, this
setting will require that no user is selected more than once on one node.

- Taxonomy fields

This module adds additional options to the administration page for each
taxonomy vocabulary (i.e. admin/structure/taxonomy/manage/<taxonomy vocabulary>)
for specifying which fields must be unique.
As like content type the administrator may specify whether each field must
be unique or whether the fields in combination must be unique and
control other options of it, same as above.

- User Fields

This module adds additional options to the account settings
page (i.e. admin/config/people/accounts).
The administrator can control the unique field settings for
user account fields (i.e. fields added in admin/config/people/accounts/fields).


## Maintainers

- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
- Joe Turgeon - [arithmetric](https://www.drupal.org/u/arithmetric)
- Immanuel Paul - [immanuel.paul](https://www.drupal.org/u/immanuelpaul)
- Logeshwaran Ashokkumar - [Logesh waran](https://www.drupal.org/u/logesh-waran)
- Pravin raj - [Pravin Ajaaz](https://www.drupal.org/u/pravin-ajaaz)
- Sidhewar Sekaran - [sidheswar](https://www.drupal.org/u/sidheswar)
- Rakesh James - [rakesh.gectcr](https://www.drupal.org/u/rakeshgectcr)
- utiks - [utiks](https://www.drupal.org/u/utiks)
